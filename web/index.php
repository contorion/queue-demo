<?php

use Symfony\Component\HttpFoundation\Response;

require_once __DIR__.'/../vendor/autoload.php';

$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../config/dev.php';

$app->get(
    '/',
    function () use ($app) {
        $twig = $app['twig'];
        return $twig->render('index.twig', array());
    }
)
    ->bind('homepage');

$app->get(
    '/queues',
    function () use ($app) {
        $admin = new \Contorion\Queue\QueueAdmin();
        $twig = $app['twig'];
        return $twig->render('queues.twig', array('queues' => $admin->getQueueInformation()));
    }
)->bind('queues');

$app->error(
    function (\Exception $e, $code) use ($app) {
        if ($app['debug']) {
            return null;
        }
        $templates = array(
            'errors/' . $code . '.twig',
            'errors/' . substr($code, 0, 2) . 'x.twig',
            'errors/' . substr($code, 0, 1) . 'xx.twig',
            'errors/default.twig',
        );
        return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
    }
);

$app->run();
