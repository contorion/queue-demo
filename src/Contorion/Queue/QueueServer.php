<?php

namespace Contorion\Queue;

use PhpAmqpLib\Connection\AMQPConnection;

class QueueServer
{
    const QUEUE_PERSISTENT = 'persistent';
    const QUEUE_TRANSIENT = 'transient';

    /**
     * @var \PhpAmqpLib\Connection\AMQPConnection
     */
    protected $connection;

    /**
     * @var \PhpAmqpLib\Channel\AMQPChannel
     */
    protected $channel;

    /**
     * @param $host
     * @param $port
     * @param $user
     * @param $password
     * @param $vhost
     */
    public function __construct($host, $port, $user, $password, $vhost)
    {
        $this->connection = new AMQPConnection($host, $port, $user, $password, $vhost);
        $this->channel = $this->connection->channel();
    }

    /**
     * @param $name
     * @param string $persistentQueue
     */
    public function declareQueue($name, $persistentQueue = self::QUEUE_PERSISTENT)
    {
        $persistent = $persistentQueue == self::QUEUE_PERSISTENT;
        $this->channel->queue_declare($name, false, $persistent, false, false);
    }

    /**
     * @param $queueName
     * @return Queue
     */
    public function getQueue($queueName)
    {
        $queue = new Queue($queueName, $this->channel);
        return $queue;
    }

}
