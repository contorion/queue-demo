<?php

namespace Contorion\Queue;

interface DataObjectInterface
{
    /**
     * @return mixed
     */
    public function getId();

    public function addToExecutionLog($name);

}
