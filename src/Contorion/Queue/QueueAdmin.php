<?php

namespace Contorion\Queue;

use Contorion\Config;
use Guzzle\Http\Client;

class QueueAdmin
{

    /**
     * @param bool $raw
     * @return mixed
     */
    public function getQueueInformation($raw = false)
    {
        $data = $this->request('queues', $raw);

        return $data;
    }

    /**
     * @param $path
     * @param bool $raw
     * @return mixed
     */
    protected function request($path, $raw = false)
    {
        $config = Config::getInstance();

        $client = new Client($config['queue.admin'] . '/api/' . $path);
        $request = $client->createRequest('GET');
        $request->setAuth($config['queue.user'], $config['queue.password']);

        $response = $request->send();
        if ($raw) {
            return $response->getBody(true);
        }

        return json_decode($response->getBody(true));
    }


}
