<?php

namespace Contorion\Queue;


use Contorion\Config;

class QueueFactory
{
    function __construct(Config $config)
    {
        $this->config = $config;
    }


    /**
     * @var QueueServer[]
     */
    protected static $queueBuffer = null;

    /**
     * @param $queueName
     * @return Queue
     */
    public function createQueue($queueName)
    {
        if (!isset(self::$queueBuffer[$queueName])) {
            $queueServer = $this->createQueueServer();

            $queueServer->declareQueue($queueName);

            self::$queueBuffer[$queueName] = $queueServer;
        }

        $queue = self::$queueBuffer[$queueName]->getQueue($queueName);
        return $queue;
    }

    /**
     * @return QueueServer
     */
    protected function createQueueServer()
    {
        $queueServer = new QueueServer(
            $this->config['queue.host'],
            $this->config['queue.port'],
            $this->config['queue.user'],
            $this->config['queue.password'],
            $this->config['queue.vhost']
        );
        return $queueServer;
    }

}
