<?php

namespace Contorion;


/**
 * Class Config
 * @package Contorion
 */
class Config extends \ArrayObject {

    /**
     * @var Config
     */
    private static $_instance;

    /**
     * @return Config
     */
    public static function getInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
            self::$_instance->initialize();
        }
        return self::$_instance;
    }

    /**
     *
     */
    protected function initialize() {
        $config = include __DIR__ . '/../../config/config.php';
        $this->exchangeArray($config);
    }

}
