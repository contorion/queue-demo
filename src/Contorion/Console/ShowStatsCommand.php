<?php

namespace Contorion\Console;

use Contorion\Queue\QueueAdmin;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShowStatsCommand extends Command {

    protected function configure()
    {
        $this->setName('queue:stats')
            ->setDescription('Show queue statistics');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $admin = new QueueAdmin();

        $queueInformation = $admin->getQueueInformation();

        $headers = ['Queue Name', 'State', 'Consumers', 'Ready', 'Unack\'d', 'Total'];
        $rows = [];


        foreach ($queueInformation as $qi) {
            $rows[$qi->name] = [$qi->name, $qi->state, $qi->consumers, $qi->messages_ready, $qi->messages_unacknowledged, $qi->messages];
        }

        ksort($rows);
        $rows = array_values($rows);

        $sum = count($rows) > 0 ? array_sum(array_column($rows, count($rows[0]) -1)) : 0;

        $rows[] = new TableSeparator();
        $rows[] = ['Total', '', '', '', '', $sum];

        $table = new Table($output);
        $table->setHeaders($headers);
        $table->setRows($rows);
        $table->render();
    }

}
