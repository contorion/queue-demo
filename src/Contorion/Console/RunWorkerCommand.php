<?php

namespace Contorion\Console;

use Contorion\Worker\WorkerFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RunWorkerCommand
 * @package Contorion\Console
 */
class RunWorkerCommand extends Command
{

    /**
     *
     */
    protected function configure()
    {
        $this->setName('worker:start')
            ->setDescription('Run a queue worker')
            ->addArgument('task', InputArgument::REQUIRED, 'The task to be executed by the worker')
            ->addOption('background', 'b', InputOption::VALUE_NONE, 'Start the worker in the background');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $taskName = $input->getArgument('task');
        $output->writeln('<info>Starting ' . $taskName . '</info>');

        $worker = WorkerFactory::createWorkerForTaskName('demo.task.' . $taskName, $taskName, 'demo.supervisor', new ConsoleLogger($output));
        $worker->setTimeout(60);
        $worker->setLogger(new ConsoleLogger($output));
        $worker->work();
    }

}
