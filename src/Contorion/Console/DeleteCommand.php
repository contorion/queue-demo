<?php

namespace Contorion\Console;

use Contorion\Config;
use Contorion\Queue\QueueAdmin;
use Contorion\Queue\QueueFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteCommand extends Command
{

    protected function configure()
    {
        $this->setName('queue:delete')
            ->setDescription('Delete Queues');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $admin = new QueueAdmin();

        $queueInformation = $admin->getQueueInformation();

        foreach ($queueInformation as $qi) {
            $queueFactory = new QueueFactory(Config::getInstance());
            $queue = $queueFactory->createQueue($qi->name);
            $output->write('Deleting ' . $qi->name . '...', false, OutputInterface::VERBOSITY_VERBOSE);
            $queue->delete();
            $output->writeln(' done', OutputInterface::VERBOSITY_VERBOSE);
        }
    }

}
