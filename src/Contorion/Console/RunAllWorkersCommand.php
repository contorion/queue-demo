<?php

namespace Contorion\Console;

use Contorion\Config;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RunAllWorkersCommand
 * @package Contorion\Console
 */
class RunAllWorkersCommand extends Command
{

    /**
     *
     */
    protected function configure()
    {
        $this->setName('worker:start-all')
            ->setDescription('Run all configured queue workers');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tasks = array_keys(Config::getInstance()['tasks']);

        foreach ($tasks as $taskName) {
            $output->writeln('<info>Starting ' . $taskName . '</info>');
            system($_SERVER['argv'][0] . ' worker:start ' . $taskName . ' >>/dev/null 2>&1 &');
        }
    }

}
