<?php

namespace Contorion\Console;

use Contorion\Example\Task\Supervisor;
use Contorion\Worker\WorkerFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RunWorkerCommand
 * @package Contorion\Console
 */
class RunSupervisorCommand extends Command
{

    /**
     *
     */
    protected function configure()
    {
        $this->setName('supervisor:start')
            ->setDescription('Run a queue worker')
            ->addOption('background', 'b', InputOption::VALUE_NONE, 'Start the worker in the background')
            ->addOption('loglevel', 'l', InputOption::VALUE_OPTIONAL, 'Log level', 'info');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Starting Supervisor</info>');

        $supervisor = WorkerFactory::createWorker('demo.supervisor', new Supervisor());
        $supervisor->setTimeout(60);
        $supervisor->setLogger(new ConsoleLogger($output));
        $supervisor->work();
    }

}
