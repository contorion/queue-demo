<?php

namespace Contorion\Worker;

use Contorion\Config;
use Contorion\Queue\QueueFactory;
use Contorion\Task\TaskInterface;
use Contorion\Task\WarmUpInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class WorkerFactory
{

    public static function createWorkerForTaskName($inputQueueName, $taskName, $outputQueueName, LoggerInterface $logger)
    {
        $config = Config::getInstance();

        /** @var TaskInterface $task */
        $taskConfig = isset($config['tasks'][$taskName]) ? $config['tasks'][$taskName] : null;

        if (!isset($taskConfig)) {
            throw new \LogicException('Could not find config for task name ' . $taskName);
        }
        $taskClass = $taskConfig['class'];
        $params = isset($taskConfig['params']) ? $taskConfig['params'] : [];
        $task = new $taskClass($params);
        if ($task instanceof LoggerAwareInterface) {
            /** @var $task LoggerAwareInterface */
            $task->setLogger($logger);
        }

        return self::createWorker($inputQueueName, $task, $outputQueueName);
    }

    /**
     * @param string $inputQueueName
     * @param TaskInterface $task
     * @param string $outputQueueName
     * @return Worker
     */
    public static function createWorker($inputQueueName, TaskInterface $task, $outputQueueName = null)
    {

        $config = Config::getInstance();

        $queueFactory = new QueueFactory($config);
        $inputQueue = $queueFactory->createQueue($inputQueueName);

        $outputQueue = null;
        if (isset($outputQueueName)) {
            $outputQueue = $queueFactory->createQueue($outputQueueName);
        }

        if ($task instanceof WarmUpInterface) {
            /** @var WarmUpInterface $task */
            $task->warmUp();
        }

        $worker = new Worker($inputQueue, $task, $outputQueue);
        $worker->setLogger(new NullLogger());

        return $worker;
    }

}
