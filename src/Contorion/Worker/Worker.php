<?php

namespace Contorion\Worker;

use Contorion\Queue\DataObjectInterface;
use Contorion\Queue\Queue;
use Contorion\Queue\QueueMessage;
use Contorion\Task\TaskInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Class Worker
 * @package Contorion\Queue
 */
class Worker implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var Queue
     */
    protected $inputQueue;

    /**
     * @var Queue
     */
    protected $responseQueue;

    /**
     * @var Queue
     */
    protected $errorQueue;

    /**
     * @var int
     */
    protected $processedMessages;

    /**
     * @var int
     */
    protected $maxMessages;

    /**
     * @var int
     */
    protected $timeout = 10;

    /**
     * @var int
     */
    protected $fetchSize = 10;

    /**
     * @param Queue $inputQueue
     * @param TaskInterface $task
     * @param Queue $responseQueue
     */
    public function __construct(Queue $inputQueue, TaskInterface $task, Queue $responseQueue = null)
    {
        $this->inputQueue = $inputQueue;
        $this->responseQueue = $responseQueue;
        $this->task = $task;
        $this->taskName = $task->getName();
    }

    /**
     * @param Queue $errorQueue
     */
    public function setErrorQueue(Queue $errorQueue)
    {
        $this->errorQueue = $errorQueue;
    }

    /**
     *
     */
    public function work()
    {
        $worker = $this;
        $this->processedMessages = 0;
        $this->inputQueue->setTimeout($this->timeout);
        $this->inputQueue->setFetchSize($this->fetchSize);
        $this->inputQueue->listen(
            function ($queueMessage) use ($worker) {
                $worker->call($queueMessage);
            },
            'Worker ' . $this->taskName
        );
    }

    /**
     * @param QueueMessage $queueMessage
     * @return bool
     */
    protected function call(QueueMessage $queueMessage)
    {
        try {
            $this->executeTask($queueMessage->getDataObject());
            $this->logger->debug(
                sprintf(
                    'Completed Task %s for message with id %s',
                    $this->task->getName(),
                    $queueMessage->getId()
                )
            );
            $this->logger->debug(print_r($queueMessage->getDataObject(), true));
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf('%s: %s%s%s', $queueMessage->getId(), $e->getMessage(), PHP_EOL, $e->getTraceAsString())
            );

            // put the item on the error queue, if configured
            if (!is_null($this->errorQueue)) {
                $queueMessage->setError($e->getMessage());
                $this->errorQueue->publish($queueMessage);
            }

            $this->inputQueue->acknowledge($queueMessage);

            return false;
        }

        if (!is_null($this->responseQueue)) {
            $this->responseQueue->publish($queueMessage);
        }

        $this->inputQueue->acknowledge($queueMessage);

        $this->processedMessages++;

        if ($this->processedMessages % 1000 == 0) {
            $this->logger->notice('processed ' . $this->processedMessages . ' messages');
        }

        if ($this->maxMessages > 0 && $this->processedMessages >= $this->maxMessages) {
            $this->logger->info('Reached maximum message count of ' . $this->maxMessages . ', stopping');
            $this->inputQueue->stopListen();
        }

        return true;
    }

    /**
     * @param DataObjectInterface $dataObject
     */
    protected function executeTask(DataObjectInterface $dataObject)
    {
        $dataObject->addToExecutionLog($this->taskName);
        $this->task->run($dataObject);
    }

    /**
     * @param int $fetchSize
     */
    public function setFetchSize($fetchSize)
    {
        $this->fetchSize = $fetchSize;
    }

    /**
     * @param int $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

}
