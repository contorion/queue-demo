<?php

namespace Contorion\Task;

interface WarmUpInterface
{
    public function warmUp();
}
