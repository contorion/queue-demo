<?php

namespace Contorion\Task;

use Contorion\Queue\DataObjectInterface;

interface TaskInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param DataObjectInterface $dataObject
     * @return bool
     */
    public function run(DataObjectInterface $dataObject);
}
