<?php

namespace Contorion\Example\Task;

use Contorion\Queue\DataObjectInterface;
use Contorion\Task\TaskInterface;

class Fibonacci implements TaskInterface
{
    use DataObjectValidationTrait;

    /**
     * @return string
     */
    public function getName()
    {
        return "fibonacci";
    }

    /**
     * @param DataObjectInterface $dataObject
     * @return DataObjectInterface
     */
    public function run(DataObjectInterface $dataObject)
    {
        $dataObject = $this->checkType($dataObject);

        $dataObject->addResult('fibonacci', $this->fibonacci($dataObject->getNumber()));
    }

    /**
     * @param $a
     * @return int
     */
    protected function fibonacci($a) {
        if ($a == 1 || $a == 2) {
            return 1;
        } else {
            return $this->fibonacci($a-1) + $this->fibonacci($a - 2);
        }
    }

}
