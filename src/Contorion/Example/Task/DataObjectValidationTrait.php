<?php

namespace Contorion\Example\Task;


use Contorion\Example\ExampleDataObject;
use Contorion\Queue\DataObjectInterface;

trait DataObjectValidationTrait
{

    /**
     * @param DataObjectInterface $dataObject
     * @return ExampleDataObject
     * @throws \LogicException
     */
    protected function checkType(DataObjectInterface $dataObject)
    {
        if (!$dataObject instanceof ExampleDataObject) {
            throw new \LogicException('Expected ExampleDataObject, got ' . get_class($dataObject));
        }

        return $dataObject;
    }

}
