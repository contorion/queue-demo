<?php

namespace Contorion\Example\Task;


use Contorion\Config;
use Contorion\Queue\DataObjectInterface;
use Contorion\Queue\Queue;
use Contorion\Queue\QueueFactory;
use Contorion\Queue\QueueMessage;
use Contorion\Task\TaskInterface;
use Contorion\Task\WarmUpInterface;

class Supervisor implements TaskInterface, WarmUpInterface
{

    use DataObjectValidationTrait;

    /**
     * @var QueueFactory
     */
    protected $queueFactory;

    /**
     * @var array
     */
    protected $queues;

    /**
     * @return string
     */
    public function getName()
    {
        return 'supervisor';
    }

    /**
     * @param DataObjectInterface $dataObject
     * @return bool
     * @throws \LogicException
     */
    public function run(DataObjectInterface $dataObject)
    {
        $dataObject = $this->checkType($dataObject);

        $taskList = $dataObject->getTaskList();

        if (count($taskList) == 0) {
            return true;
        }

        $task = array_shift($taskList);
        $dataObject->setTaskList($taskList);

        $queue = $this->getQueue('demo.task.' . $task);
        $queueMessage = new QueueMessage();
        $queueMessage->setDataObject($dataObject);
        $queue->publish($queueMessage);

        return true;
    }

    public function warmUp()
    {
        $this->queueFactory = new QueueFactory(Config::getInstance());
    }

    /**
     * @param $queueName
     * @return Queue
     */
    protected function getQueue($queueName)
    {
        if (!isset($this->queues[$queueName])) {
            $this->queues[$queueName] = $this->queueFactory->createQueue($queueName);
        }

        return $this->queues[$queueName];
    }


}
