<?php

namespace Contorion\Example\Task;

use Contorion\Queue\DataObjectInterface;
use Contorion\Task\TaskInterface;

class Output implements TaskInterface
{
    use DataObjectValidationTrait;

    /**
     * @return string
     */
    public function getName()
    {
        return "output";
    }

    /**
     * @param DataObjectInterface $dataObject
     * @return DataObjectInterface
     */
    public function run(DataObjectInterface $dataObject)
    {
        $dataObject = $this->checkType($dataObject);

        echo $dataObject . PHP_EOL;
    }

}
