<?php

namespace Contorion\Example\Task;

use Contorion\Queue\DataObjectInterface;
use Contorion\Task\TaskInterface;
use Contorion\Task\WarmUpInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class WarmFibonacci implements TaskInterface, WarmUpInterface, LoggerAwareInterface
{
    use DataObjectValidationTrait;
    use LoggerAwareTrait;

    /**
     * @var array
     */
    protected $fibonaccis;

    /**
     * @return string
     */
    public function getName()
    {
        return "fibonacci";
    }

    /**
     * @param DataObjectInterface $dataObject
     * @return DataObjectInterface
     */
    public function run(DataObjectInterface $dataObject)
    {
        $dataObject = $this->checkType($dataObject);

        $dataObject->addToExecutionLog($this->getName());

        $dataObject->addResult('fibonacci', $this->fibonaccis[$dataObject->getNumber()]);
    }

    /**
     *
     */
    public function warmUp()
    {
        $this->logger->info('warming up fibonacci sequence');
        $this->fibonaccis[0] = 0;
        $this->fibonaccis[1] = 1;
        $this->fibonaccis[2] = 1;
        for ($i = 3; $i <= 10000000; $i++) {
            $this->fibonaccis[$i] = $this->fibonaccis[$i-1] + $this->fibonaccis[$i-2];
        }
        $this->logger->info('finished');
    }


}
