<?php

namespace Contorion\Example\Task;

use Contorion\Queue\DataObjectInterface;
use Contorion\Task\TaskInterface;

class Sleeper implements TaskInterface
{
    use DataObjectValidationTrait;

    protected $interval;

    public function __construct(array $params) {
        assert(isset($params['interval']));
        $this->interval = $params['interval'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "sleeper";
    }

    /**
     * @param DataObjectInterface $dataObject
     * @return DataObjectInterface
     */
    public function run(DataObjectInterface $dataObject)
    {
        $dataObject = $this->checkType($dataObject);

        usleep($this->interval * 1000);
    }

}
