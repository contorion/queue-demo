<?php

namespace Contorion\Example;

use Contorion\Config;
use Contorion\Queue\QueueFactory;
use Contorion\Queue\QueueMessage;

class Filler
{
    protected $queue;

    public function __construct()
    {
        $factory = new QueueFactory(Config::getInstance());
        $this->queue = $factory->createQueue('demo.supervisor');
    }

    public function putOneOnQueue($id)
    {
        // make the process plan
        $tasks = Config::getInstance()['tasks'];
        unset($tasks['supervisor']);
        unset($tasks['output']);
        $tasks = array_keys($tasks);
        shuffle($tasks);
        $tasks[] = 'output';
        // /plan

        $dataObject = new ExampleDataObject($id);
        $dataObject->setNumber(rand(0, 10000));
        $dataObject->setTaskList($tasks);

        $queueMessage = new QueueMessage($dataObject);
        $queueMessage->setId($dataObject->getId());

        $this->queue->publish($queueMessage);
    }

}
