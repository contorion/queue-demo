<?php

namespace Contorion\Example;

use Contorion\Queue\DataObjectInterface;

class ExampleDataObject implements DataObjectInterface, \JsonSerializable
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $number;

    /**
     * @var array
     */
    protected $results;

    /**
     * @var array
     */
    protected $taskList;

    /**
     * @var array
     */
    protected $executionLog;

    /**
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param array $taskList
     */
    public function setTaskList(array $taskList)
    {
        $this->taskList = $taskList;
    }

    /**
     * @return array
     */
    public function getTaskList()
    {
        return $this->taskList;
    }

    /**
     * @return array
     */
    public function getExecutionLog()
    {
        return $this->executionLog;
    }

    /**
     * @param $message
     */
    public function addToExecutionLog($message)
    {
        $this->executionLog[] = $message;
    }

    /**
     * @return array
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param $name
     * @param $result
     */
    public function addResult($name, $result)
    {
        $this->results[$name] = $result;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this, JSON_PRETTY_PRINT);
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        $array = [];
        foreach ($this as $variable => $value) {
            if ($variable == 'results' && is_array($value)) {
                $value = array_map(
                    function ($v) {
                        return $v == INF ? 'INF' : $v;
                    },
                    $value
                );
            }
            $array[$variable] = $value;
        }

        return $array;
    }

}
