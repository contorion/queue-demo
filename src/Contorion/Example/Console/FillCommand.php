<?php

namespace Contorion\Example\Console;

use Contorion\Example\Filler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FillCommand extends Command
{

    protected function configure()
    {
        $this->setName('queue:fill')
            ->setDescription('Fill the supervisor queue with random data');

        $this->addOption('amount', 'a', InputOption::VALUE_OPTIONAL, 'The number of messages to produce', 10000);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $amount = $input->getOption('amount');

        $filler = new Filler();
        $progress = new ProgressBar($output, $amount);
        $progress->start();

        for ($id = 1; $id <= $amount; $id++) {

            $filler->putOneOnQueue($id);
            if ($id % 100 == 0) {
                $progress->advance(100);
            }
            usleep(100);
        }
        $progress->finish();
        $output->writeln('');
        $output->writeln('<info>filled the supervisor queue</info>');
    }

}
