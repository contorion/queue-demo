<?php

$config['queue.host'] = '127.0.0.1';
$config['queue.port'] = '5672';
$config['queue.vhost'] = '/';
$config['queue.user'] = 'guest';
$config['queue.password'] = 'guest';
$config['queue.admin'] = 'http://' . $config['queue.host'] . ':15672';

$config['tasks'] = [
//    'fibonacci1' => ['class' => 'Contorion\Example\Task\WarmFibonacci'],
//    'fibonacci2' => ['class' => 'Contorion\Example\Task\WarmFibonacci'],
//    'fibonacci3' => ['class' => 'Contorion\Example\Task\Fibonacci'],
//    'fibonacci4' => ['class' => 'Contorion\Example\Task\Fibonacci'],
//    'fibonacci5' => ['class' => 'Contorion\Example\Task\Fibonacci'],
    'sleeper1' => ['class' => 'Contorion\Example\Task\Sleeper', 'params' => ['interval' => 10]],
    'sleeper2' => ['class' => 'Contorion\Example\Task\Sleeper', 'params' => ['interval' => 20]],
    'sleeper3' => ['class' => 'Contorion\Example\Task\Sleeper', 'params' => ['interval' => 40]],
    'sleeper4' => ['class' => 'Contorion\Example\Task\Sleeper', 'params' => ['interval' => 80]],
    'sleeper5' => ['class' => 'Contorion\Example\Task\Sleeper', 'params' => ['interval' => 250]],
    'output' => ['class' => 'Contorion\Example\Task\Output'],
];

return $config;
