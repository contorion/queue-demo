Queue Demo
==========

This project demonstrates the queue handling as it was presented on the code.talks 2014 conference in Hamburg.

To use it, follow these simple instructions:

### rabbitmq

 Make sure to have rabbitmq-server and php installed on your machine. See [the RabbitMQ website](http://www.rabbitmq.com/) for instructions how to install. Leave the default user 'guest' with password 'guest' intact.

### Enable the rabbitmq management plugin and restart rabbitmq

    sudo rabbitmq-plugins enable rabbitmq_management
    sudo /etc/init.d/rabbitmq-server restart

### Clone the project to your local drive

    git clone https://bitbucket.org/contorion/queue-demo.git

### Run composer install

    cd queue-demo
    ./composer.phar install

### Fill the supervisor queue

    bin/console queue:fill

### check the stats of the queues

    bin/console queue:stats

### run the demo workers

    bin/console worker:start-all

or run one specifically

    bin/console worker:start fibonacci1

### start the supervisor

    bin/console supervisor:start
